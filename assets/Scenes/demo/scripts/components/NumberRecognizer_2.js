let _ = require('lodash');
let DollarRecognizer = require('pdoallarplus');

cc.Class({
    extends: cc.Component,

    properties: {
        gestureJsonAsset: cc.JsonAsset,

        threshold: 3.5,
        _numberConfig: null,
        _strokes: null,
        _numbers: null, //目标数字（写书的可能是这里面的某个数）
        _input: '',
    },

    onLoad() {
        this._strokes = [];
        this._numbers = [];
        this._pdrOne = new DollarRecognizer();
        this._pdrTwo = new DollarRecognizer();

        //初始化手势数据
        this.numberConfig = this.gestureJsonAsset.json;
        _.forEach(this.numberConfig, (array, key) => {
            let points = array.map(p => {
                p.ID = p.ID || 0;
                return p;
            });
            if (!CC_EDITOR) {
                if (key === '4' || key === '5' || key === 'x') {
                    this._pdrTwo.AddGesture(key, points);
                }
                this._pdrOne.AddGesture(key, points);
            }
        });
    },

    /**
     * 用于外部增加手势
     * @param {*} name 
     * @param {*} points 
     */
    addGesture(name, points) {
        //编辑器状态_pdrOne不存在
        if (name === '4' || name === '5' || name === 'x') {
            if (this._pdrTwo) {
                this._pdrTwo.AddGesture(name, points);
            }
        }

        if (this._pdrOne) {
            this._pdrOne.AddGesture(name, points);
        }
    },

    /**
     * 清除延时提交
     */
    _clearDelayEmit() {
        this.unschedule(this._emitResult);
        //cc.log('取消延时提交');
    },

    /**
     * 清除当前识别任务
     */
    clear() {
        //cc.log('识别器清除');
        this._clearDelayEmit();
        this._strokes = [];
        this._input = '';
    },

    _convertPoints(points) {
        let ID = this._strokes.length;
        points.forEach(p => p.ID = ID);
        this._strokes.push(points);
    },

    /**
     * 识别点数组
     * @param {*} points 
     */
    recognize(points) {
        if (points.length < 2) {
            cc.log('点位不足退出', points.length);
            return;
        }
        this._convertPoints(points);

        this._clearDelayEmit();

        let result;
        //单笔识别
        if (this._strokes.length === 1) {
            result = this._pdrOne.Recognize(this.getPoints());
        } else {
            //两笔识别
            result = this._pdrTwo.Recognize(this.getPoints());
        }

        //let result = this._pdrOne.Recognize(this.getPoints(), true);
        cc.log('识别结果:', result.Name, result.Time, result.Score, this._strokes.length);


        //4和5需要输入两笔,等待输入
        // if ((result.Name === '5' || result.Name === '4' || result.Name === 'x') && this._strokes.length === 1) {
        //     return;
        // }

        //小于阀值，识别成功，发送事件
        if (result.Score < this.threshold) {
            this._input += result.Name;
            this._emitResult(result);
            return;
        }

        //大于阀值，检查识别结果是否在numbers中，存在则提交
        let isOK = this._checkInNumber(result.Name);
        if (isOK) {
            this._emitResult(result);
            return;
        }

        //超时，等待继续输入，1后秒发送事件
        if (this._strokes.length >= 2) {
            this._strokes = [];
            this._emitResult(result);
        } else {
            cc.log('等待1秒后提交输入');
            this._result = result;
            this.scheduleOnce(this._emitResult, 0.1);
        }
    },

    clearAll() {
        this.clear();
        this.node.$TouchGraphics.clear();
        this.node.emit('clear');
        //this._writeAudio.pause();
    },



    /**
     * 检查识别结果是否存在
     * @param {*} input 
     */
    _checkInNumber(input) {
        let temp = this._input + input;
        let value = _.find(this._numbers, (str) => {
            return str.startsWith(temp) || str.startsWith(input);
        });
        if (value) {
            this._input += input;
        }
        //cc.log(`_checkInNumber numbers:${this._numbers.join(',')}, input:${input}, value:${value}`);
        return !!value;
    },

    /**
     * 发送当前提交结果
     * @param {*} result 
     */
    _emitResult(result) {
        this._strokes = [];
        if (_.isObject(result)) {
            //根据名字判断发送什么消息
            if (this.node.name == '_recognizer_digit') {
                cc.log('发送消息' + 'recognized_digit')
                this.node.emit('recognized_digit', result);
            } else {
                cc.log('发送消息' + 'recognized_tens')
                this.node.emit('recognized_tens', result);
            }
            return;
        }
        //cc.log('触发延时提交', this._result.Name);
        if (this.node.name == '_recognizer_digit') {
            this.node.emit('recognized_digit', this._result);
        } else {
            this.node.emit('recognized_tens', this._result);
        }
        //this.node.emit('recognized', this._result);
    },

    /**
     * 获取采样点
     */
    getPoints() {
        let array = _.flatten(this._strokes);
        return array;
    },

    /**
     * 设置即将输入的数字，提高识别率
     * @param {*} values 
     */
    setNumbers(values) {
        this._numbers = values;
    }
});

