/**
 * 触摸绘图
*/

let async = require('async');
let _ = require('lodash');

let TouchGraphics = cc.Class({
    extends: cc.Graphics,

    properties: {
        _points: null,
    },

    onLoad() {
        this.node.on(cc.Node.EventType.TOUCH_START, this._onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this._onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this._onTouchEnd, this);
        this.leftLimit = this.node.x - 100;
        this.rightLimit = this.node.x + 100;
        this.upLimit = this.node.y + 150;
        this.downLimit = this.node.y - 150;
    },
    posInLimit(pos) {
        if (pos.x < this.rightLimit
            && pos.x > this.leftLimit
            && pos.y < this.upLimit
            && pos.y > this.downLimit) {
            cc.log('true')
            return true;
        } else {
            cc.log('false')
            return false;
        }
    },

    _onTouchStart(event) {
        this._points = [];
        let location = this.node.convertToNodeSpaceAR(event.getLocation());
        this._drawGraphics(location);
    },

    drawGraphics(location) {
        this._drawGraphics(location);
    },

    /**
     * 绘制线条，根据参数点
     * @param {*} pointArray 
     */
    drawLine(pointArray) {
        pointArray.forEach((point, index) => {
            if (index === 0) {
                this.moveTo(point.x, point.y);
            } else {
                this.lineTo(point.x, point.y);
            }
        });
        this.stroke();
    },

    _onTouchMove(event) {
        let location = this.node.convertToNodeSpaceAR(event.getLocation());
        if (this.posInLimit(location)) {
            this._drawGraphics(location);
        }
        //cc.log(location);
    },

    _onTouchEnd(event) {
        let location = this.node.convertToNodeSpaceAR(event.getLocation());
        this._drawGraphics(location);
        this.node.emit('draw-end', this);
    },

    getTrailPoints() {
        return this._points || [];
    },

    revoke(finished) {
        let points = this.getTrailPoints();
        let length = points.length;
        let t = 0.1;
        async.eachSeries(_.range(0, length), (index, cb) => {
            this.clear();
            length -= 4;
            if (length <= 0) {
                cb(1);
                return;
            }
            let array = points.slice(0, length);
            cc.log(length, array.length);
            this.drawLine(array);
            this.scheduleOnce(() => cb(), 0.03);
        }, finished);
    },
    //控制绘画的方法
    _drawGraphics(location) {
        let point = location;// this.node.convertToNodeSpaceAR();
        if (this._points.length === 0) {
            this.moveTo(point.x, point.y);
            this._points.push(point);
            this.node.emit('draw-start', this);
            return;
        }

        if (point.x === 0 && point.y === 0) {
            return;
        }

        let last = this._points[this._points.length - 1];
        let len = last.sub(point).mag();
        if (last && len < 4) {
            return;
        }

        this._points.push(point);
        this.lineTo(point.x, point.y);
        this.stroke();
        this.node.emit('draw-move', this);
    },
});
//事件在引擎启动后触发，此时您将能够使用所有引擎类
cc.game.once(cc.game.EVENT_ENGINE_INITED, function () {
    TouchGraphics._assembler = cc.Graphics._assembler;
});
//屏蔽一些TouchGraphics不需要的属性
cc.Class.Attr.setClassAttr(TouchGraphics, 'miterLimit', 'visible', false);

module.exports = TouchGraphics;