let _ = require('lodash');
let Thor = require('Thor');
let gestureData = require('./NumberGestureData');

cc.Class({
    extends: Thor,

    properties: {
        writeClip: { type: cc.AudioClip, default: null },
        moveClip: { type: cc.AudioClip, default: null },
        _writeAudio: null,
        img_digit: cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        //开启节点监听
        this._recognizer_digit.on('draw-start', this._onDrawStart_digit, this);
        this._recognizer_digit.on('draw-end', this._onDrawEnd_digit, this);
        this._recognizer_digit.on('recognized_digit', this._onRecognizer_digit, this);
    },

    start() {
        _.forEach(gestureData, (data) => {
            let strokes;
            if (typeof data === 'string') {
                strokes = JSON.parse(data.strokes);
            } else {
                strokes = data.strokes;
            }
            this._recognizer_digit.$NumberRecognizer.addGesture(data.name, strokes);
        });
    },


    _onDrawStart_digit() {
        cc.log('开始书写个位数，并清空之前数字')
        this.img_digit.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame();

        //this.writeID = cc.audioEngine.playEffect(this.writeClip);

        // this._writeAudio.pause();
        // this._writeAudio.currentTime = 0
        // this._writeAudio.play();
    },

    _onDrawEnd_digit(sender) {
        cc.log('个位数书写结束，识别是什么数字')
        let points = sender.getTrailPoints();
        this._recognizer_digit.$NumberRecognizer.recognize(points);
        //cc.audioEngine.pauseEffect(this.writeID);
        // this._writeAudio.currentTime = 0;
        // this._writeAudio.pause();
    },

    _onRecognizer_digit(result) {
        cc.log('识别结束，显示个位数是什么数字')
        this.clear_digit();
        this.showResult(result, this.img_digit);
        //this.node.emit('recognizer', this, result);
    },

   
    clear_digit() {
        this._recognizer_digit.$NumberRecognizer.clear();
        this._recognizer_digit.$TouchGraphics.clear();
        this.node.emit('clear');
        //this._writeAudio.pause();
    },

    /**
     * 显示结果图片
     * @param {*} result 
     * @param {*} node 
     */
    showResult(result, node) {
        cc.log('显示结果')
        cc.log(result.Name)
        var self = this;
        switch (result.Name) {
            case '0':
                cc.loader.loadRes("num/0", cc.SpriteFrame, function (err, spriteFrame) {
                    node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
                });
                break;
            case '1':
                cc.loader.loadRes("num/1", cc.SpriteFrame, function (err, spriteFrame) {
                    node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
                });
                break;
            case '2':
                cc.loader.loadRes("num/2", cc.SpriteFrame, function (err, spriteFrame) {
                    node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
                });
                break;
            case '3':
                cc.loader.loadRes("num/3", cc.SpriteFrame, function (err, spriteFrame) {
                    node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
                });
                break;
            case '4':
                cc.loader.loadRes("num/4", cc.SpriteFrame, function (err, spriteFrame) {
                    node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
                });
                break;
            case '5':
                cc.loader.loadRes("num/5", cc.SpriteFrame, function (err, spriteFrame) {
                    node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
                });
                break;
            case '6':
                cc.loader.loadRes("num/6", cc.SpriteFrame, function (err, spriteFrame) {
                    node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
                });
                break;
            case '7':
                cc.loader.loadRes("num/7", cc.SpriteFrame, function (err, spriteFrame) {
                    node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
                });
                break;
            case '8':
                cc.loader.loadRes("num/8", cc.SpriteFrame, function (err, spriteFrame) {
                    node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
                });
                break;
            case '9':
                cc.loader.loadRes("num/9", cc.SpriteFrame, function (err, spriteFrame) {
                    node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
                });
                break;
            default: cc.log('未找到')
        }
    },
});
