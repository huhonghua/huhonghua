// 加法控制器，控制基础加法的关卡实现。
cc.Class({
    extends: cc.Component,

    properties: {
        chapter_dataJson: cc.JsonAsset,
        a: cc.Label,
        b: cc.Label,
        c: cc.Label,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() { },

    start() {
        cc.log(this.chapter_dataJson.json)
        cc.log(H.chapter, H.lesson);
        this.init();
    },

    init() {
        cc.log(this.chapter_dataJson.json.easyAdd_1.chapter_1.lesson_1)
        let chapter = 'chapter_' + H.chapter;
        let lesson = 'lesson_' + H.lesson;
        let jsonPath = 'this.chapter_dataJson.json.easyAdd_1.' + chapter + lesson;
        //this.arr = this.chapter_dataJson.json.easyAdd_1.chapter.lesson;
        cc.log(jsonPath)
    },

    // update (dt) {},
});
