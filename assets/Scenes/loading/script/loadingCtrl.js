//控制loading界面
cc.Class({
    extends: Thor,

    properties: {
        progress: {
            default: null,
            type: cc.Node,
            tiptool: '进度条'
        },
        dog: cc.Node,
        count_label: cc.Label,
    },

    onLoad() {
        if (CC_EDITOR) {
            return;
        }
        this.dog.runAction(cc.moveTo(0.5, 0, -108));
        cc.director.preloadScene('draw', (completedCount, totalCount) => {
            let load = completedCount / totalCount;
            this.progress.getComponent(cc.ProgressBar).progress = load;
            this.count_label.string = load * 100 + '%';
        }, () => {
            this.scheduleOnce(function () {
                this.dog.runAction(cc.moveTo(0.5, 400, -108));
            }, 1);

            this.scheduleOnce(function () {
                cc.director.loadScene('draw');
            }, 2);

        });
    },

    start() {

    },

    // update (dt) {},
});