// 测试脚本
cc.Class({
    extends: cc.Component,

    properties: {
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
        //单元
        let unit = H.common.getUnit();
        cc.log('unit值为'+JSON.stringify(unit))

        let unitID = unit[0].id;
        cc.log('第一单元的id值是'+unitID)

        //章节
        let chapter = H.common.getChapter(unitID);
        cc.log('chapter值为'+JSON.stringify(chapter))

        let chapterID = chapter[0].id;
        cc.log('章节id值是'+chapterID);
        cc.log('章节name是'+chapter[0].name);

        //课程
        let lesson = H.common.getLesson(chapterID);
        let lessonNum = lesson.length;
        cc.log('lesson值为'+JSON.stringify(lesson))
        cc.log('课程数'+lessonNum)

    },

    // update (dt) {},
});
