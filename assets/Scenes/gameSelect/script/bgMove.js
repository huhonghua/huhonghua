//控制大背景的触摸移动
cc.Class({
    extends: cc.Component,

    properties: {
        BG: {
            default: null,
            type: cc.Node,
            tooltip: '显示的区域',
        },
        Bg_long: {
            default: null,
            type: cc.Node,
            tooltip: '长背景',
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        //cc.director.getPhysicsManager().enabled = true;
    },

    start() {
        let self = this;
        //长背景加监听
        this.Bg_long.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
            let pre = event.getPreviousLocation();//获取触点在上一次事件时的位置对象，对象包含 x 和 y 属性。
            let cur = event.getLocation();//获取当前触点位置。
            var dir = cur.sub(pre);//当前触点位置减去上次触点位置。
            //修改长背景的坐标
            self.Bg_long.x += dir.x;
            self.Bg_long.y += dir.y;
            //判断边距离
            var BGLeft = self.BG.x - self.BG.width / 2;
            var BGRight = self.BG.x + self.BG.width / 2;
            var BGTop = self.BG.y + self.BG.height / 2;
            var BGBottom = self.BG.y - self.BG.height / 2;

            var halfMapWidth = self.Bg_long.width / 2;
            var halfMapHeight = self.Bg_long.height / 2;

            //左边
            if (self.Bg_long.x - halfMapWidth > BGLeft) {
                self.Bg_long.x = BGLeft + halfMapWidth;
            }
            //右边
            if (self.Bg_long.x + halfMapWidth < BGRight) {
                self.Bg_long.x = BGRight - halfMapWidth;
            }
            //上边
            if (self.Bg_long.y + halfMapHeight < BGTop) {
                self.Bg_long.y = BGTop - halfMapHeight;
            }
            //下边
            if (self.Bg_long.y - halfMapHeight > BGBottom) {
                self.Bg_long.y = BGBottom + halfMapHeight;
            }
        });
    },

    update(dt) {

    },
});