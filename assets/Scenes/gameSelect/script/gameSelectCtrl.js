//游戏关卡选择控制，能判断该关卡有多少课程，把课程的按钮显示出来。
cc.Class({
    extends: cc.Component,

    properties: {
        chapter_countJson: cc.JsonAsset,
        select_lesson: {
            default: null,
            type: cc.Node,
            tiptool: '课程选择界面'
        },
        lesson_: {
            default: null,
            type: cc.Prefab,
            tiptool: '课程按钮预制'
        },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {
        cc.log(this.chapter_countJson.json)
    },
    /*
    * @describe 课程按钮点击事件，展开课程关卡
    */
    lesson_click(event, customEventData) {
        this.openSelectLesson();
        this.checkchapter(customEventData);
    },
    /*
    * @describe 根据传入的参数判断显示的课程
    */
    checkchapter(chapter_num) {
        let num = this.chapter_countJson.json.easyAddLevel.lessonCount;
        switch (chapter_num) {
            case '1':
                cc.log('关卡1')
                this.creatLesson(num, chapter_num)
                break;
            case '2':
                cc.log('关卡2')
                this.creatLesson(num, chapter_num)
                break;
            case '3':
                cc.log('关卡3')
                this.creatLesson(num, chapter_num)
                break;
            case '4':
                cc.log('关卡4')
                this.creatLesson(num, chapter_num)
                break;
            case '5':
                cc.log('关卡5')
                this.creatLesson(num, chapter_num)
                break;
            case '6':
                cc.log('关卡6')
                this.creatLesson(num, chapter_num)
                break;
            case '7':
                cc.log('关卡7')
                this.creatLesson(num, chapter_num)
                break;
            case '8':
                cc.log('关卡8')
                this.creatLesson(num, chapter_num)
                break;
            case '9':
                cc.log('关卡9')
                this.creatLesson(num, chapter_num)
                break;
            default: cc.warn('未找到传入参数')
        }
    },

    creatLesson(num, chapter_num) {
        for (let i = 0; i < num; i++) {
            let lesson = cc.instantiate(this.lesson_);
            lesson.parent = this.select_lesson.children[0];
            lesson.name = 'lesson_' + i;
            lesson.getComponent('lessonCtrl').init(chapter_num, i);
        }
    },

    /*
    * @describe 课程关卡出现
    */
    openSelectLesson() {
        this.select_lesson.active = true;
    },
    /*
    * @describe 课程关卡关闭
    */
    closeSelectLesson() {
        this.select_lesson.active = false;
        this.destroyLesson();
    },
    //销毁课程节点
    destroyLesson() {
        let node = this.select_lesson.children[0];
        for (let i = 0; i < node.childrenCount; i++) {
            node.children[i].destroy();
        }
    }

    // update (dt) {},
});
