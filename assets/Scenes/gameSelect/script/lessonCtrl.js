// 控制每个小课程按钮事件.需要知道的两个关键，是哪个大的关卡 level 和哪个小的课程 lesson
cc.Class({
    extends: cc.Component,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.node.on('touchend', this.onTouchEnd, this);
    },

    start() {

    },
    //初始化脚本中的变量参数
    init(chapter, lesson) {
        cc.log('当前是' + chapter + '关卡' + '中的' + lesson + '课程');
        this.chapter = chapter;
        this.lesson = lesson;
    },

    onTouchEnd() {
        cc.log('点击');
        cc.log('当前是' + this.chapter + '关卡' + '中的' + this.lesson + '课程');
        H.chapter = this.chapter;
        H.lesson = this.lesson;
        cc.director.loadScene('add');

    },

    // update (dt) {},
});
