
// var captureItem = cc.Class({
//     name: 'captureItem',
//     properties: {
//         captureName: '',

//     }
// });

cc.Class({
    extends: cc.Component,

    properties: {

    },

    // use this for initialization
    onLoad: function () {
    },

    readData: function () {
        if (cc.sys.localStorage.getItem('level') != null) {

            Global.level = cc.sys.localStorage.getItem('level')
        } else {
            cc.sys.localStorage.setItem('level', Global.level);
        }

        cc.log('游戏开始读取数据的level', Global.level);


    },

    saveUserDataEvent: function () {

        cc.sys.localStorage.setItem('level', Global.level);

        cc.log('游戏保存数据的level', Global.level);



    },




    clearUserDataEvent: function () {
        cc.sys.localStorage.clear();
        this.label_control.string = "清空用户数据";
        this.readData();
    },
    //重置游戏数据
    resetSave() {
        cc.sys.localStorage.clear();
        //cc.sys.localStorage.removeItem('localScreenshots');
        //cc.sys.localStorage.setItem('level', 1);
        //cc.sys.localStorage.removeItem('UnlockSelection');

    },

    // update: function (dt) {

    // },
});