
var common = cc.Class({

    extends: cc.Component,

    properties: {
        
    },
 
    // use this for initialization
    onLoad: function () {
        H.commonInfo = common;
        H.common = this;
    },
    
    //时间格式化
    timeFmt: function (time, fmt) { //author: meizz 
        var o = {
            "M+": time.getMonth() + 1, //月份 
            "d+": time.getDate(), //日 
            "h+": time.getHours(), //小时 
            "m+": time.getMinutes(), //分 
            "s+": time.getSeconds(), //秒 
            "q+": Math.floor((time.getMonth() + 3) / 3), //季度 
            "S": time.getMilliseconds() //毫秒 
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (time.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    },

    //生成从minNum到maxNum的随机数
    randomNum(minNum, maxNum) {
        switch (arguments.length) {
            case 1:
                return parseInt(Math.random() * minNum + 1, 10);
                break;
            case 2:
                return parseInt(Math.random() * (maxNum - minNum + 1) + minNum, 10);
                break;
            default:
                return 0;
                break;
        }
    },

     /**
     @returns array
     *根据单元返回对应的章节
     字段说明：name,单元名称。id，单元的id值。
     */
    getUnit(){
        // TODO
        // 固定数据 开发使用。后面再根据数据表处理数据
        return [{name:"基础加法1",id:1},{name:"基础加法2",id:2},{name:"基础减法1",id:3}];
    },

    /**
     @param unitID 单元ID值
     @returns array
     *根据单元返回对应的章节
     字段说明：name,章节的名称。id，章节的id值。playType章节的算法类型：1为加法、2为减法、3为乘法、4为除法。
     */
    getChapter(unitID){
        // 异常情况返回null值，使用前前先判断是否为null
        if(unitID == null){
            return  null;
        }
        // TODO
        // 固定数据 开发使用。后面再根据数据表处理数据
        return [{name:"+1",playType:1,id:1},{name:"+1",playType:1,id:2},{name:"+1",playType:1,id:3}];
    },


    /**
     *根据章节返回课程
     @param chapterID 传入章节id值
     @returns array
     字段说明：x，加减乘除的第一项。y，加减乘除的第二项。
     type，这一项的数据类型，值为normal，直接用value值进行操作。值为random，使用min和max作为随机区间取随机数
     */
    getLesson(chapterID){
        // 异常情况返回null值，使用前前先判断是否为null
        if(chapterID == null){
            return  null;
        }

        // TODO
        // 固定数据 开发使用。后面再根据数据表处理数据
        return [
            {
                x:{
                    type:"normal",
                    value:1,
                    min:0,
                    max:0
                },
                y:{
                    type:"normal",
                    value:2,
                    min:1,
                    max:10
                }
            },
            {
                x:{
                    type:"normal",
                    value:1,
                    min:0,
                    max:0
                },
                y:{
                    type:"random",
                    value:0,
                    min:1,
                    max:10
                }
            }
        ];
    },
});
