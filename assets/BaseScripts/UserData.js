var userData = {
    // 背景音乐开关 bool类型, true为开，false为关
    getMusicStatus() {
        if (cc.sys.localStorage.getItem("MusicStatus")) {
            if (cc.sys.localStorage.getItem("MusicStatus") === "1") {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    },
    // 设置背景音乐开关 bool类型, true为开，false为关
    setMusicStatus(value) {
        if (value) {
            cc.sys.localStorage.setItem("MusicStatus", "1")
        } else {
            cc.sys.localStorage.setItem("MusicStatus", "0")
        }
    },
    // 获取解锁列表
    getUnlockList() {
        if (cc.sys.localStorage.getItem("UnlockSelection") != null) {
            return JSON.parse(cc.sys.localStorage.getItem('UnlockSelection'));
        } else {
            return [];
        }
    },
    // 加入到解锁列表里
    addToUnlockList(unlockName) {
        if (cc.sys.localStorage.getItem("UnlockSelection") != null) {
            var arr = JSON.parse(cc.sys.localStorage.getItem('UnlockSelection'));
            arr.push(unlockName);
            cc.sys.localStorage.setItem('UnlockSelection', JSON.stringify(arr));
        } else {
            var arr = new Array();
            arr.push(unlockName);
            cc.sys.localStorage.setItem('UnlockSelection', JSON.stringify(arr));
        }
    },
    // 检查是否在解锁列表里
    checkInUnlockList(name) {
        var array = this.getUnlockList();
        if (array.length > 0) {
            for (let index = 0; index < array.length; index++) {
                if (array[index] === name) {
                    return true;
                }
            }
        }
        return false;
    }
}

//导出为引用模块
module.exports = userData