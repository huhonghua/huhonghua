//该脚本包含全部共用音频文件，用到时候直接调用即可
cc.Class({
    extends: cc.Component,

    properties: {
        // //*********************** 音效  ************************
        // //主背景音
        BGM: {
            default: null,
            type: cc.AudioClip,
            tooltip: '主背景音',
        },
        bm: {
            default: null,
            type: cc.AudioClip,
            tooltip: '打开出租车、货车、公车、垃圾车背景音乐',
        },
        bj: {
            default: null,
            type: cc.AudioClip,
            tooltip: '打开消防、警车、救护车的背景音乐',
        },
        boarding: {
            default: null,
            type: cc.AudioClip,
            tooltip: '出租车场景：点击行李 公车场景：点击人物上车 货车场景：点击货物音效',
        },
        close: {
            default: null,
            type: cc.AudioClip,
            tooltip: '公交、出租车、警车关门音效',
        },
        Garbage: {
            default: null,
            type: cc.AudioClip,
            tooltip: '垃圾车、大货车行驶时的音效',
        },
        startDrive: {
            default: null,
            type: cc.AudioClip,
            tooltip: '所有车辆开启前都会启动发动机，会有音效',
        },
        Auxiliary: {
            default: null,
            type: cc.AudioClip,
            tooltip: '人物拖动到出租车内、乘客推进公交车、床拖到救护车的音效',
        },
        open: {
            default: null,
            type: cc.AudioClip,
            tooltip: '所有车开门的音效',
        },
        click: {
            default: null,
            type: cc.AudioClip,
            tooltip: '点击开始、关闭、切换种类、返回',
        },
        FiretaxiBus: {
            default: null,
            type: cc.AudioClip,
            tooltip: '出租车、公交车、消防车行驶有音效',
        },
        laba: {
            default: null,
            type: cc.AudioClip,
            tooltip: '喇叭声',
        },

        //***********************  音效  ************************
        // this.one = cc.audioEngine.play(Global.GameAudio().one, false, 1);
    },

});
