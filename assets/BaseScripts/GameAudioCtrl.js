var GameAudio = require("GameAudio");

cc.Class({
    extends: cc.Component,

    properties: {
        gameAudio: {
            default: null,
            type: GameAudio,
            visible: false
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        cc.game.addPersistRootNode(this.node);
        cc.log("cc.sys.languageCode  " + cc.sys.languageCode);
        if (cc.sys.language === 'zh') {
            this.gameAudio = this.node.getChildByName("zh").getComponent('GameAudio');
        } else {
            this.gameAudio = this.node.getChildByName("en").getComponent('GameAudio');
        }
        // this.gameAudio = this.node.getChildByName("en").getComponent('GameAudio');
    },

    start() { },
    // update (dt) {},

    audio() {
        return this.gameAudio;
    }
});
